﻿#include <iostream>
#include <math.h>

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;

public:
    Vector()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    double Module()
    {
        return sqrt(x*x, y*y, z*z);
    }
    void Show()
    {
        std::cout << "\n" << x << ' ' << y << ' ' << z;
    }
};

int main()
{
   Vector v(2,2,2);
   v.Show();
   v.Module();
}